(function () {

    var cardAmt = 20; //10 cards but all of them x2
    var cardRow = 5;
    var cards = [];
    var cardsPicked = [];
    var cardToTake = true;
    var steps = 0;
    var cardPair = 0;
    var cardPics = [
        "sailor1.png",
        "sailor2.png",
        "sailor3.png",
        "sailor4.png",
        "sailor5.png",
        "sailor6.png",
        "sailor7.png",
        "sailor8.png",
        "sailor9.png",
        "sailor10.png",
    ];

    function startGame() {
        cards = [];
        cardsPicked = [];
        cardToTake = true;
        steps = 0;
        cardPair = 0;

        var board = $('.board').empty();

        for (var i = 0; i < cardAmt; i++) {

            cards.push(Math.floor(i / 2));
        }

        for (i = cardAmt - 1; i > 0; i--) {
            var swap = Math.floor(Math.random() * i);
            var tmp = cards[i];
            cards[i] = cards[swap];
            cards[swap] = tmp;

        }

        for (i = 0; i < cardAmt; i++) {

            var tile = $('<div class="card"></div>');
            board.append(tile);
            tile.data('cardType', cards[i]);
            tile.data('index', i);
            tile.css({
                left: 5 + (tile.width() + 5) * (i % cardRow)
            });
            tile.css({
                top: 5 + (tile.height() + 5) * (Math.floor(i / cardRow))
            });
            tile.bind('click', function () {
                cardClick($(this))
            });
        }
        $('.moves').html(steps);
    }


    function cardClick(element) {

        if (cardToTake) {
            //if the 1st element is not picked 
            //or index of the element is not in picked
            if (!cardsPicked[0] || (cardsPicked[0].data('index') != element.data('index')))
                cardsPicked.push(element);
            element.css({
                'background-image': 'url(' + cardPics[element.data('cardType')] + ')'
            })

        }

        if (cardsPicked.length == 2) {

            cardToTake = false;

            if (cardsPicked[0].data('cardType') == cardsPicked[1].data('cardType')) {
                window.setTimeout(function () {
                    cardDelete();
                }, 500);
            } else {
                window.setTimeout(function () {
                    card_reset();
                }, 500);
            }
            steps++;
            $('.moves').html(steps)
        }
    }

    function cardDelete() {
        cardsPicked[0].fadeOut(function () {
            $(this).remove();
        });
        cardsPicked[1].fadeOut(function () {
            $(this).remove();

            cardPair++;
            if (cardPair >= cardAmt / 2) {

                alert('You won! :)');
            }
            cardToTake = true;
            cardsPicked = new Array();
        });
    }

    function card_reset() {

        cardsPicked[0].css({
            'background-image': 'url(sailorMain.png)'
        })
        cardsPicked[1].css({
            'background-image': 'url(sailorMain.png)'
        })
        cardsPicked = new Array();
        cardToTake = true;
    }

    $(document).ready(function () {

        $('.start').click(function () {
            startGame();
        });

    })
})();